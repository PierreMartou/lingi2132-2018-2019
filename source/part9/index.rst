.. _part9:

*************************************************************************************************
Partie 9 | Traits and monads
*************************************************************************************************

Question proposed by Group 31, Jean Bosco Rwibutso and  Roman Skubiszewski
=====================================================================

Question 1. Write a code allowing to create a Stack class with methods put and get that extends at least two traits. The behavior of the class is to add 0 in the stack if the put value is even and add 1 if the put value is odd. Each case is handled in its specific trait (Even and Odd handler).
------------


Question 2. Translate these following line codes into match case : 
------------
.. code-block:: scala   
    a. listVariable1.flatMap(f1(_)) 
	b. listVariable2.flatten
	c. listVariable3.map(f3(_))
	d. listVariable4.foreach(f4(_))

Question 3.  Explain the difference between the methods orElse and getOrElse on an Option.
------------
